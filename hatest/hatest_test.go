package hatest

import (
	"fmt"
	"io"
	"net/http"
	"testing"
	"time"

	"git.autistici.org/ale/hostauth/client"
	"git.autistici.org/ale/hostauth/util"
)

var testServerPort = 18231

func startTestServer(t testing.TB, c *client.Client) string {
	httpserver := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, "hello")
	})

	addr := fmt.Sprintf("%s:%d", util.Hostname, testServerPort)
	testServerPort++
	l, err := client.Listen(addr, c)
	if err != nil {
		t.Fatalf("Listen(): %v", err)
	}
	go func() {
		if err := http.Serve(l, httpserver); err != nil {
			t.Fatalf("http.Serve(): %v", err)
		}
	}()
	return addr
}

func TestHostauthTest_SimpleServer(t *testing.T) {
	hc, err := CreateContext()
	if err != nil {
		t.Fatalf("CreateContext(): %v", err)
	}
	defer hc.Close()

	c, err := hc.NewClient()
	if err != nil {
		t.Fatalf("NewClient(): %v", err)
	}

	serverAddr := startTestServer(t, c)

	tlsConfig, err := c.ClientTlsConfig(serverAddr)
	if err != nil {
		t.Fatalf("ClientTlsConfig(): %v", err)
	}
	tr := &http.Transport{
		TLSClientConfig: tlsConfig,
	}
	httpClient := &http.Client{Transport: tr}
	resp, err := httpClient.Get(fmt.Sprintf("https://%s/", serverAddr))
	if err != nil {
		t.Fatalf("http.Get(): %v", err)
	}
	defer resp.Body.Close()
}

func TestHostauthTest_ServerCertRenew(t *testing.T) {
	hc, err := CreateContext()
	if err != nil {
		t.Fatalf("CreateContext(): %v", err)
	}
	defer hc.Close()

	hc.hostca.ServerCertTimeout = 10 * time.Millisecond

	c, err := hc.NewClient()
	if err != nil {
		t.Fatalf("NewClient(): %v", err)
	}
	startTestServer(t, c)

	time.Sleep(100 * time.Millisecond)
	if hc.hostca.ServerCertCount < 5 {
		t.Fatalf("CA did not sign enough certificates (%d, exp >= 5). Perhaps the renewal mechanism isn't working")
	}
}
