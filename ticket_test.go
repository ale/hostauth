package hostauth

import (
	"crypto/x509"
	"testing"
	"time"
)

type testKeyLocator struct {
	crt *x509.Certificate
}

func (l *testKeyLocator) Get(hostname string) (*x509.Certificate, error) {
	return l.crt, nil
}

func TestTicket_CreateAndVerify(t *testing.T) {
	ca, _ := CreateRootCA()

	tkt := &Ticket{
		Credentials: NewCredentials(),
		Deadline:    time.Now().Add(3600 * time.Second),
	}
	s, err := tkt.Sign(ca.PrivateKey)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("ticket = %s", s)

	tkt2, err := ParseTicket(s, &testKeyLocator{ca.Certificate})
	if err != nil {
		t.Fatal(err)
	}
	if tkt2.Credentials != tkt.Credentials {
		t.Fatalf("error decoding ticket, got=%#v want=%#v", tkt2, tkt)
	}
}
