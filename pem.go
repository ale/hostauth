package hostauth

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/asn1"
	"encoding/pem"
	"errors"
	"fmt"
	"strings"
	"time"
)

const (
	pemCertificateType = "CERTIFICATE"
	pemECPrivateKeyType = "EC PRIVATE KEY"
)

// ExpirationDeadline returns the time when we should renew the given
// certificate.
func ExpirationDeadline(crt *x509.Certificate) time.Time {
	now := time.Now().UTC()
	// If the certificate is already expired, just return its expiration time.
	if crt.NotAfter.Before(now) {
		return crt.NotAfter.Local()
	}
	d := crt.NotAfter.Sub(now)
	d = time.Duration(d.Seconds()*0.8*float64(time.Second))
	return now.Add(d).Local()
}

// EncodeCertificatePEM encodes one or more certificates in PEM
// format.
func EncodeCertificatePEM(certs... *x509.Certificate) []byte {
	var buf bytes.Buffer
	for _, c := range certs {
		pem.Encode(&buf, &pem.Block{Type: pemCertificateType, Bytes: c.Raw})
	}
	return buf.Bytes()
}

// EncodePrivateKeyPEM encodes a private key in PEM format.
func EncodePrivateKeyPEM(priv *ecdsa.PrivateKey) []byte {
	der, err := MarshalPrivateKey(priv)
	if err != nil {
		panic(err)
	}
	return pem.EncodeToMemory(&pem.Block{Type: pemECPrivateKeyType, Bytes: der})
}

// DecodeCertificatePEM reads a single X509 certificate encoded in PEM
// format from the given data.
func DecodeCertificatePEM(data []byte) (*x509.Certificate, error) {
	block, _ := pem.Decode(data)
	if block == nil {
		return nil, errors.New("no PEM block found in input")
	}
	if block.Type != pemCertificateType {
		return nil, errors.New("encoded object is not a certificate")
	}
	certs, err := x509.ParseCertificates(block.Bytes)
	if err != nil {
		return nil, err
	}
	return certs[0], nil
}

// DecodePrivateKeyPEM reads a PEM-encoded private key.
func DecodePrivateKeyPEM(data []byte) (*ecdsa.PrivateKey, error) {
	block, _ := pem.Decode(data)
	if block == nil {
		return nil, errors.New("no PEM block found in input")
	}
	if block.Type != pemECPrivateKeyType {
		return nil, errors.New("encoded object is not an EC private key")
	}
	return x509.ParseECPrivateKey(block.Bytes)
}

// CertificateToString returns a printable summary of the certificate details.
func CertificateToString(crt *x509.Certificate) string {
	var out []string
	if crt.IsCA {
		out = append(out, "ca")
	} else if len(crt.ExtKeyUsage) > 0 {
		switch crt.ExtKeyUsage[0] {
		case x509.ExtKeyUsageServerAuth:
			out = append(out, "server")
		case x509.ExtKeyUsageClientAuth:
			out = append(out, "client")
		}
	}
	out = append(out, fmt.Sprintf("subj=%s", PKIXNameToString(crt.Subject)))
	if len(crt.DNSNames) > 0 {
		out = append(out, fmt.Sprintf("dns=%s", strings.Join(crt.DNSNames, ",")))
	}
	if len(crt.IPAddresses) > 0 {
		var ips []string
		for _, ip := range crt.IPAddresses {
			ips = append(ips, ip.String())
		}
		out = append(out, fmt.Sprintf("ips=%s", strings.Join(ips, ",")))
	}
	return fmt.Sprintf("%x (%s)", crt.SerialNumber, strings.Join(out, "; "))
}

// CertificateRequestToString returns a printable summary of the CSR details.
func CertificateRequestToString(csr *x509.CertificateRequest) string {
	out := []string{fmt.Sprintf("subj=%s", PKIXNameToString(csr.Subject))}
	if len(csr.DNSNames) > 0 {
		out = append(out, fmt.Sprintf("dns=%s", strings.Join(csr.DNSNames, ",")))
	}
	if len(csr.IPAddresses) > 0 {
		var ips []string
		for _, ip := range csr.IPAddresses {
			ips = append(ips, ip.String())
		}
		out = append(out, fmt.Sprintf("ips=%s", strings.Join(ips, ",")))
	}
	return strings.Join(out, "; ")
}

// CertificateChainToString returns a human-readable representation of
// a certificate chain.
func CertificateChainToString(chain []*x509.Certificate) string {
	var items []string
	for _, crt := range chain {
		items = append(items, PKIXNameToString(crt.Subject))
	}
	return strings.Join(items, " -> ")
}

var (
	oidOrganization       = asn1.ObjectIdentifier{2, 5, 4, 10}
	oidOrganizationalUnit = asn1.ObjectIdentifier{2, 5, 4, 11}
	oidCommonName         = asn1.ObjectIdentifier{2, 5, 4, 3}
	oidLocality           = asn1.ObjectIdentifier{2, 5, 4, 7}
)

func pkixTypeName(attrType asn1.ObjectIdentifier) string {
	switch {
	case attrType.Equal(oidOrganization):
		return "O"
	case attrType.Equal(oidOrganizationalUnit):
		return "OU"
	case attrType.Equal(oidLocality):
		return "L"
	case attrType.Equal(oidCommonName):
		return "CN"
	default:
		return attrType.String()
	}
}

// PKIXNameToString formats a pkix.Name object to a human-readable
// string.
func PKIXNameToString(name pkix.Name) string {
	var out []string
	for _, seq := range name.ToRDNSequence() {
		for _, p := range seq {
			out = append(out, fmt.Sprintf("%s=%s", pkixTypeName(p.Type), p.Value))
		}
	}
	return strings.Join(out, "/")
}
