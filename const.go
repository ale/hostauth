package hostauth

import "crypto/tls"

var CipherSuites = []uint16{
	// Preferred cipher for TLS 1.2.
	tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
	// Fallback ECDSA cipher for TLS 1.0.
	tls.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA,
}
