package hostauth

import "testing"

func TestCredentials_PKIX(t *testing.T) {
	creds := NewCredentials()
	creds2, err := NewCredentialsFromName(creds.Name(""))
	if err != nil {
		t.Fatal(err)
	}
	if creds != creds2 {
		t.Errorf("PKIX name conversion mismatch: orig=%#v, result=%#v", creds, creds2)
	}
}
