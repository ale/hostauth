package hostauth

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"fmt"
	"io/ioutil"
	"math/big"
	"net"
	"time"
)

var (
	rootOrgName = []string{"hostauth"}
	rootName    = "Root CA"
)

func MarshalPrivateKey(key *ecdsa.PrivateKey) ([]byte, error) {
	return x509.MarshalECPrivateKey(key)
}

// A certification authority.
type CA struct {
	PrivateKey  *ecdsa.PrivateKey
	Certificate *x509.Certificate

	CaCertTimeout     time.Duration
	ServerCertTimeout time.Duration
	ClientCertTimeout time.Duration

	// Some statistics, mostly useful for testing.
	ServerCertCount int
	ClientCertCount int
}

const (
	defaultCaCertTimeout     = 7 * 24 * time.Hour
	defaultServerCertTimeout = 26 * time.Hour
	defaultClientCertTimeout = 2 * time.Hour
)

// NewCA returns a new CA with the given private and public keys. CA
// parameters are initialized to defaults.
func NewCA(key *ecdsa.PrivateKey, cert *x509.Certificate) *CA {
	return &CA{
		PrivateKey:        key,
		Certificate:       cert,
		CaCertTimeout:     defaultCaCertTimeout,
		ServerCertTimeout: defaultServerCertTimeout,
		ClientCertTimeout: defaultClientCertTimeout,
	}
}

// NewCAFromBytes creates a CA using the given credentials, which are
// expected to be ASN-1 encoded (DER).
func NewCAFromBytes(key, cert []byte) (*CA, error) {
	k, err := x509.ParseECPrivateKey(key)
	if err != nil {
		return nil, err
	}
	c, err := x509.ParseCertificate(cert)
	if err != nil {
		return nil, err
	}
	return NewCA(k, c), nil
}

// LoadCA returns a new CA loading its credentials from the specified
// files. Certificate and key must be encoded in PEM format.
func LoadCA(keyPath, crtPath string) (*CA, error) {
	crtData, err := ioutil.ReadFile(crtPath)
	if err != nil {
		return nil, err
	}
	crt, err := DecodeCertificatePEM(crtData)
	if err != nil {
		return nil, err
	}
	keyData, err := ioutil.ReadFile(keyPath)
	if err != nil {
		return nil, err
	}
	key, err := DecodePrivateKeyPEM(keyData)
	if err != nil {
		return nil, err
	}
	return NewCA(key, crt), nil
}

// CreateCertificateRequest creates a CSR.
func CreateCertificateRequest(name pkix.Name, altNames []string, ips []net.IP) (*x509.CertificateRequest, *ecdsa.PrivateKey, error) {
	priv, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to generate ECDSA key: %v", err)
	}
	csr, err := CreateCertificateRequestWithKey(name, altNames, ips, priv)
	if err != nil {
		return nil, nil, err
	}
	return csr, priv, nil
}

// CreateCertificateRequestWithKey creates a CSR signed by the given
// private key.
func CreateCertificateRequestWithKey(name pkix.Name, altNames []string, ips []net.IP, priv *ecdsa.PrivateKey) (*x509.CertificateRequest, error) {
	req := x509.CertificateRequest{
		Subject:            name,
		IPAddresses:        ips,
		DNSNames:           altNames,
		SignatureAlgorithm: x509.ECDSAWithSHA256,
	}
	b, err := x509.CreateCertificateRequest(rand.Reader, &req, priv)
	if err != nil {
		return nil, err
	}
	return x509.ParseCertificateRequest(b)
}

// Marshal returns a DER-format representation of the private and
// public keys of the CA.
func (ca *CA) Marshal() ([]byte, []byte, error) {
	keyData, err := MarshalPrivateKey(ca.PrivateKey)
	if err != nil {
		return nil, nil, err
	}
	crtData := ca.Certificate.Raw
	return keyData, crtData, nil
}

// CreateCA generates and signs a new intermediate CA certificate.
func (ca *CA) CreateCA(csr *x509.CertificateRequest) (*x509.Certificate, error) {
	usage := x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign
	extUsage := []x509.ExtKeyUsage{}
	return createCert(csr, usage, extUsage, ca.Certificate, ca.PrivateKey, true, ca.CaCertTimeout)
}

// CreateServerCert generates and signs a new server certificate.
func (ca *CA) CreateServerCert(csr *x509.CertificateRequest) (*x509.Certificate, error) {
	ca.ServerCertCount++
	usage := x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature
	extUsage := []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth}
	return createCert(csr, usage, extUsage, ca.Certificate, ca.PrivateKey, false, ca.ServerCertTimeout)
}

// CreateClientCert generates and signs a new client certificate.
func (ca *CA) CreateClientCert(csr *x509.CertificateRequest) (*x509.Certificate, error) {
	ca.ClientCertCount++
	usage := x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature
	extUsage := []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth}
	return createCert(csr, usage, extUsage, ca.Certificate, ca.PrivateKey, false, ca.ClientCertTimeout)
}

var serialNumberLimit = new(big.Int).Lsh(big.NewInt(1), 128)

func certTemplate(name pkix.Name, serialNumber *big.Int, validity time.Duration, usage x509.KeyUsage, extUsage []x509.ExtKeyUsage, isCA bool) *x509.Certificate {
	now := time.Now().UTC()
	return &x509.Certificate{
		SerialNumber:          serialNumber,
		Subject:               name,
		NotBefore:             now.Add(-5 * time.Minute),
		NotAfter:              now.Add(validity),
		SignatureAlgorithm:    x509.ECDSAWithSHA256,
		KeyUsage:              usage,
		ExtKeyUsage:           extUsage,
		BasicConstraintsValid: true,
		IsCA: isCA,
	}
}

// CreateRootCA generates a self-signed root certification authority.
func CreateRootCA() (*CA, error) {
	key, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		return nil, fmt.Errorf("failed to generate ECDSA key: %v", err)
	}

	name := pkix.Name{
		Organization: rootOrgName,
		CommonName:   rootName,
	}
	usage := x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign
	template := certTemplate(name, big.NewInt(1), 10*365*24*time.Hour, usage, []x509.ExtKeyUsage{}, true)

	template.MaxPathLen = 2

	b, err := x509.CreateCertificate(rand.Reader, template, template, key.Public(), key)
	if err != nil {
		return nil, fmt.Errorf("could not sign certificate: %v", err)
	}

	crt, err := x509.ParseCertificate(b)
	if err != nil {
		return nil, err
	}

	return NewCA(key, crt), nil
}

func createCert(csr *x509.CertificateRequest, usage x509.KeyUsage, extUsage []x509.ExtKeyUsage, signerPub *x509.Certificate, signerPriv interface{}, isCA bool, validity time.Duration) (*x509.Certificate, error) {
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		return nil, fmt.Errorf("failed to generate serial number: %v", err)
	}

	template := certTemplate(csr.Subject, serialNumber, validity, usage, extUsage, isCA)
	template.DNSNames = csr.DNSNames
	template.IPAddresses = csr.IPAddresses
	if isCA {
		template.MaxPathLen = 1
	}

	b, err := x509.CreateCertificate(
		rand.Reader,
		template,
		signerPub,
		csr.PublicKey,
		signerPriv,
	)
	if err != nil {
		return nil, err
	}

	return x509.ParseCertificate(b)
}
