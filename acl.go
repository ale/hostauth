package hostauth

import (
	"errors"
	"fmt"
	"strings"
)

type ACLContext interface {
	UserInGroup(u, g string) bool
	HostInGroup(h, g string) bool
}

type aclMatcher interface {
	Match(Credentials, ACLContext) bool
}

type ACL interface {
	// Match credentials against the access control list, using
	// the provided context to obtain group information.
	//
	// With user and group-based access controls, an empty
	// corresponding field in the Credentials will be treated as a
	// wildcard, so all user and group matches will return true
	// (Credentials with an empty User act as host-specific
	// wildcards, the ones with empty Host act as user-specific
	// wildcards).
	Match(Credentials, ACLContext) bool
}

type ruleList []ACL

type ruleListOr ruleList

func (l ruleListOr) Match(creds Credentials, ctx ACLContext) bool {
	for _, r := range l {
		if r.Match(creds, ctx) {
			return true
		}
	}
	return false
}

type ruleListAnd ruleList

func (l ruleListAnd) Match(creds Credentials, ctx ACLContext) bool {
	for _, r := range l {
		if !r.Match(creds, ctx) {
			return false
		}
	}
	return true
}

type aclUserMatch struct {
	user string
}

func (a *aclUserMatch) Match(creds Credentials, ctx ACLContext) bool {
	return creds.User == a.user || creds.User == ""
}

type aclGroupMatch struct {
	group string
}

func (a *aclGroupMatch) Match(creds Credentials, ctx ACLContext) bool {
	return creds.User == "" || ctx.UserInGroup(creds.User, a.group)
}

type aclHostMatch struct {
	host string
}

func (a *aclHostMatch) Match(creds Credentials, ctx ACLContext) bool {
	return creds.Host == a.host || creds.Host == ""
}

type aclHostGroupMatch struct {
	group string
}

func (a *aclHostGroupMatch) Match(creds Credentials, ctx ACLContext) bool {
	return creds.Host == "" || ctx.HostInGroup(creds.Host, a.group)
}

func parseACLRule(s string) (ACL, error) {
	var userSpec, hostSpec string
	if strings.Contains(s, "@") {
		sp := strings.SplitN(s, "@", 2)
		userSpec = sp[0]
		hostSpec = sp[1]
	} else {
		userSpec = s
	}

	var rules []ACL
	if strings.HasPrefix(userSpec, "%") {
		group := strings.TrimPrefix(userSpec, "%")
		if group == "" {
			return nil, errors.New("empty group")
		}
		rules = append(rules, &aclGroupMatch{group: group})
	} else if userSpec != "" && userSpec != "*" {
		rules = append(rules, &aclUserMatch{user: userSpec})
	}
	if strings.HasPrefix(hostSpec, "%") {
		hostgroup := strings.TrimPrefix(hostSpec, "%")
		if hostgroup == "" {
			return nil, errors.New("empty host group")
		}
		rules = append(rules, &aclHostGroupMatch{group: hostgroup})
	} else if hostSpec != "" && hostSpec != "*" {
		rules = append(rules, &aclHostMatch{host: hostSpec})
	}
	return ruleListAnd(rules), nil
}

func ParseACL(s string) (ACL, error) {
	var rules []ACL
	for _, part := range strings.Split(s, ",") {
		part = strings.TrimSpace(part)
		rule, err := parseACLRule(part)
		if err != nil {
			return nil, fmt.Errorf("error parsing '%s': %v", part, err)
		}
		rules = append(rules, rule)
	}
	return ruleListOr(rules), nil
}
