package util

import "testing"

func TestLocalhost_Show(t *testing.T) {
	t.Logf("primary host name: %s", Hostname)
	t.Logf("local host names: %v", Hostnames)
	t.Logf("local ip addrs: %v", LocalIPs)
}
