package util

import (
	"reflect"
	"testing"
	"time"
)

func TestScheduler(t *testing.T) {
	now := time.Now()

	s := NewScheduler()
	s.Add("two", now.Add(75*time.Millisecond))
	s.Add("one", now.Add(50*time.Millisecond))
	s.Add("three", now.Add(100*time.Millisecond))

	var result []string
	for name := range s.C {
		result = append(result, name)
		if len(result) == 3 {
			s.Close()
		}
	}
	expected := []string{"one", "two", "three"}
	if !reflect.DeepEqual(result, expected) {
		t.Fatalf("got %v, want %v", result, expected)
	}
}

func TestScheduler_Update(t *testing.T) {
	now := time.Now()

	// Updates on the same key should generate a single event.
	s := NewScheduler()
	s.Add("one", now.Add(75*time.Millisecond))
	s.Add("one", now.Add(50*time.Millisecond))
	s.Add("one", now.Add(20*time.Millisecond))

	c := time.After(100 * time.Millisecond)
	go func() {
		<-c
		s.Close()
	}()

	var result []string
	for name := range s.C {
		result = append(result, name)
	}
	expected := []string{"one"}
	if !reflect.DeepEqual(result, expected) {
		t.Fatalf("got %v, want %v", result, expected)
	}
}

func TestScheduler_InThePast(t *testing.T) {
	now := time.Now()
	s := NewScheduler()
	s.Add("one", now.Add(-3600*time.Second))

	go func() {
		time.Sleep(100 * time.Millisecond)
		s.Close()
	}()

	var result []string
	for name := range s.C {
		result = append(result, name)
	}
	expected := []string{"one"}
	if !reflect.DeepEqual(result, expected) {
		t.Fatalf("got %v, want %v", result, expected)
	}
}
