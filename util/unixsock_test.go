package util

import (
	"bytes"
	"io/ioutil"
	"net"
	"os"
	"testing"
)

func TestUnixServer_Raw(t *testing.T) {
	h := UnixHandlerFunc(func(conn *Conn) {
		t.Logf("connection %x from pid=%d, uid=%s", conn.RequestID, conn.Credentials.Pid, conn.Credentials.User)
		data, err := ioutil.ReadAll(conn)
		if err != nil {
			t.Errorf("server read error: %v", err)
		}
		_, err = conn.Write(data)
		if err != nil {
			t.Errorf("server write error: %v", err)
		}
	})
	path := ".test_socket"
	defer os.Remove(path)

	s := NewUnixServer(h)
	go s.ListenAndServe(path)
	defer s.Close()

	s.WaitUntilReady()

	addr, _ := net.ResolveUnixAddr("unix", path)
	conn, err := net.DialUnix("unix", nil, addr)
	if err != nil {
		t.Fatal(err)
	}
	hello := []byte("hello")
	conn.Write(hello)
	conn.CloseWrite()
	b := make([]byte, 5)
	conn.Read(b)
	if !bytes.Equal(hello, b) {
		t.Errorf("client read error: got %v", b)
	}
	conn.Close()
}

type TestRequest struct {
	Tags []string
}

type TestResponse struct {
	Result string
}

func TestUnixServer_Objects(t *testing.T) {
	h := UnixHandlerFunc(func(conn *Conn) {
		t.Logf("connection %x from pid=%d, uid=%s", conn.RequestID, conn.Credentials.Pid, conn.Credentials.User)
		var request TestRequest
		var response TestResponse

		if err := conn.ReadObj(&request); err != nil {
			t.Fatalf("server ReadObj error: %v", err)
		}
		response.Result = "ok"
		if err := conn.WriteObj(&response); err != nil {
			t.Fatalf("server WriteObj error: %v", err)
		}
	})
	path := ".test_socket"
	defer os.Remove(path)

	s := NewUnixServer(h)
	go s.ListenAndServe(path)
	defer s.Close()

	s.WaitUntilReady()

	conn, err := DialUNIX(path)
	if err != nil {
		t.Fatal(err)
	}

	request := TestRequest{
		Tags: []string{"foo", "bar"},
	}
	var response TestResponse

	if err := conn.WriteObj(&request); err != nil {
		t.Fatalf("client WriteObj error: %v", err)
	}
	if err := conn.ReadObj(&response); err != nil {
		t.Fatalf("client ReadObj error: %v", err)
	}

	if response.Result != "ok" {
		t.Fatalf("bad response: %+v", response)
	}

	conn.Close()
}
