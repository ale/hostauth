package nssutil

/*
#cgo CPPFLAGS: -D_GNU_SOURCE
#include <unistd.h>
#include <sys/types.h>
#include <grp.h>
#include <pwd.h>
#include <netdb.h>
*/
import "C"
import (
	"fmt"
	"sync"
	"time"
	"unsafe"
)

type cacheDatum struct {
	data    interface{}
	error   error
	expires time.Time
}

// Cache for NSS-related results. All NSS lookups are controlled by
// the global mutex, since some functions (for instance setnetgrent /
// endnetgrent) are inherently thread-unsafe.
type nssCache struct {
	sync.Mutex
	cache map[string]cacheDatum
}

var cacheTtl = 600 * time.Second

func newNssCache() *nssCache {
	return &nssCache{cache: make(map[string]cacheDatum)}
}

var globalNssCache *nssCache

func init() {
	globalNssCache = newNssCache()
}

func (c *nssCache) Get(bucket, key string, fn func(string) (interface{}, error)) (interface{}, error) {
	ckey := fmt.Sprintf("%s.%s", bucket, key)
	c.Lock()
	datum, ok := c.cache[ckey]
	if !ok || time.Now().After(datum.expires) {
		data, err := fn(key)
		datum = cacheDatum{
			data:    data,
			error:   err,
			expires: time.Now().Add(cacheTtl),
		}
		c.cache[ckey] = datum
	}
	c.Unlock()
	return datum.data, datum.error
}

func doGetGroupList(username string) map[string]struct{} {
	var ngroups C.int
	var pw C.struct_passwd
	var pwres *C.struct_passwd
	pwbuf := make([]byte, C.sysconf(C._SC_GETPW_R_SIZE_MAX))

	C.getpwnam_r(C.CString(username), &pw, (*C.char)(unsafe.Pointer(&pwbuf[0])), C.size_t(len(pwbuf)), &pwres)
	if pwres == nil {
		return nil
	}

	ngroups = 8
	var groups []C.__gid_t
	for {
		groups = make([]C.__gid_t, ngroups)
		r := C.getgrouplist(C.CString(username), pwres.pw_gid, &groups[0], &ngroups)
		if r >= 0 {
			break
		}
	}

	out := make(map[string]struct{})
	grbuf := make([]byte, C.sysconf(C._SC_GETGR_R_SIZE_MAX))
	for i := 0; i < int(ngroups); i++ {
		var gr C.struct_group
		var grres *C.struct_group
		C.getgrgid_r(groups[i], &gr, (*C.char)(unsafe.Pointer(&grbuf[0])), C.size_t(len(grbuf)), &grres)
		if grres != nil {
			out[C.GoString(grres.gr_name)] = struct{}{}
		}
	}
	return out
}

func GetGroupList(username string) map[string]struct{} {
	result, err := globalNssCache.Get("usergroups", username, func(username string) (interface{}, error) {
		groups := doGetGroupList(username)
		return groups, nil
	})
	if err != nil {
		return nil
	}
	return result.(map[string]struct{})
}

type NetgroupSpec struct {
	User, Host, Domain string
}

func doGetNetgroup(netgroup string) []NetgroupSpec {
	if r := C.setnetgrent(C.CString(netgroup)); r == 0 {
		return nil
	}
	defer C.endnetgrent()

	buf := make([]byte, 256)
	var out []NetgroupSpec
	for {
		var hostp, userp, domainp *C.char
		if r := C.getnetgrent_r(&hostp, &userp, &domainp, (*C.char)(unsafe.Pointer(&buf[0])), C.size_t(len(buf))); r == 0 {
			break
		}
		out = append(out, NetgroupSpec{
			User:   C.GoString(userp),
			Host:   C.GoString(hostp),
			Domain: C.GoString(domainp),
		})
	}
	return out
}

func GetNetgroup(netgroup string) []NetgroupSpec {
	result, err := globalNssCache.Get("netgroup", netgroup, func(netgroup string) (interface{}, error) {
		entries := doGetNetgroup(netgroup)
		return entries, nil
	})
	if err != nil {
		return nil
	}
	return result.([]NetgroupSpec)
}

func GetHostsInNetgroup(netgroup string) map[string]struct{} {
	result, err := globalNssCache.Get("netgroup", netgroup, func(netgroup string) (interface{}, error) {
		out := make(map[string]struct{})
		for _, e := range doGetNetgroup(netgroup) {
			if e.Host != "" {
				out[e.Host] = struct{}{}
			}
		}
		return out, nil
	})
	if err != nil {
		return nil
	}
	return result.(map[string]struct{})

}

func UserInGroup(user, group string) bool {
	groups := GetGroupList(user)
	_, ok := groups[group]
	return ok
}

func HostInGroup(host, netgroup string) bool {
	hosts := GetHostsInNetgroup(netgroup)
	_, ok := hosts[host]
	return ok
}
