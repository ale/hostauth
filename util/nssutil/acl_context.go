package nssutil

type NSSAclContext struct{}

func (c *NSSAclContext) HostInGroup(host, group string) bool {
	return HostInGroup(host, group)
}

func (c *NSSAclContext) UserInGroup(user, group string) bool {
	return UserInGroup(user, group)
}

func NewNSSAclContext() *NSSAclContext {
	return &NSSAclContext{}
}
