package nssutil

import (
	"os/user"
	"testing"
)

func mapToList(m map[string]struct{}) []string {
	var out []string
	for k := range m {
		out = append(out, k)
	}
	return out
}

func TestNSS_GetGroupList(t *testing.T) {
	me, _ := user.Current()
	groups := GetGroupList(me.Username)
	t.Logf("groups = %#v", mapToList(groups))
	if len(groups) == 0 {
		t.Fatal("no groups returned from getgrouplist()")
	}

	groups = GetGroupList("hopefully_does_not_exist")
	if groups != nil {
		t.Fatal("non-empty value for nonexisting user: %#v", groups)
	}
}

func TestNSS_GetNetgroup(t *testing.T) {
	entries := GetNetgroup("testgroup")
	if len(entries) > 0 {
		t.Logf("netgroup 'testgroup' = %#v", entries)
	} else {
		t.Log("netgroup 'testgroup' has no entries")
	}
}
