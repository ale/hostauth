package hostauth

import (
	"crypto/x509"
	"testing"
	"time"
)

func TestExpirationDeadline(t *testing.T) {
	// Test with an expiration time in the past.
	t1 := time.Now().Add(-10 * time.Second)
	exp := ExpirationDeadline(&x509.Certificate{
		NotAfter: t1.UTC(),
	})
	if !t1.Equal(exp) {
		t.Fatalf("ExpirationDeadline(now) != now (diff: %s", t1.Sub(exp))
	}

	now := time.Now()
	t2 := now.Add(300 * time.Second)
	exp = ExpirationDeadline(&x509.Certificate{
		NotAfter: t2.UTC(),
	})
	// Result should fall within the range of 'now' and 't2'.
	if !exp.After(now) || !exp.Before(t2) {
		t.Fatalf("result %s out of range %s - %s", exp.Format(time.RFC3339), now.Format(time.RFC3339), t2.Format(time.RFC3339))
	}
}
