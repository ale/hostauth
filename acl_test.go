package hostauth

import "testing"

type aclTest struct {
	spec   string
	result bool
}

type fakeCtx struct {
	groups     map[string][]string
	hostgroups map[string][]string
}

func (f *fakeCtx) UserInGroup(user, group string) bool {
	values, ok := f.groups[group]
	if ok {
		for _, v := range values {
			if v == user {
				return true
			}
		}
	}
	return false
}

func (f *fakeCtx) HostInGroup(host, group string) bool {
	values, ok := f.hostgroups[group]
	if ok {
		for _, v := range values {
			if v == host {
				return true
			}
		}
	}
	return false
}

func TestACL_Match(t *testing.T) {
	testdata := []aclTest{
		{"*", true},
		{"", true},
		{"user1", true},
		{"user2", false},
		{"user1@host1", true},
		{"user1@host2", false},
		{"*@host1", true},
		{"*@host2", false},
		{"%group1", true},
		{"%group2", false},
		{"%unknowngroup", false},
		{"%group1@host1", true},
		{"%group2@host1", false},
		{"%group1@host2", false},
		{"user1@%hostgroup1", true},
		{"user2@%hostgroup1", false},
		{"user1@%unknowngroup", false},
		{"*@%hostgroup1", true},
		{"@%hostgroup1", true},
		{"*@%hostgroup2", false},
		{"%group1@%hostgroup1", true},
		{"%group1@%hostgroup2", false},

		// Test multiple ACLs.
		{"user1,user2,user3", true},
		{"user1@host1,user1@host2", true},
		{"user1@host2,user2@host1", false},
	}

	creds := Credentials{
		User: "user1",
		Host: "host1",
	}

	testACLs(t, testdata, creds)
}

func TestACL_MatchUserWildcard(t *testing.T) {
	testdata := []aclTest{
		{"*", true},
		{"user1", true},
		{"*@host1", true},
		{"*@host2", false},
		{"user1@%hostgroup1", true},
		{"@%hostgroup2", false},
	}

	creds := Credentials{Host: "host1"}

	testACLs(t, testdata, creds)
}

func testACLs(t *testing.T, testdata []aclTest, testCreds Credentials) {
	ctx := &fakeCtx{
		groups: map[string][]string{
			"group1": []string{"user1", "user2"},
			"group2": []string{"user2"},
		},
		hostgroups: map[string][]string{
			"hostgroup1": []string{"host1", "host2"},
			"hostgroup2": []string{"host2"},
		},
	}

	for _, td := range testdata {
		acl, err := ParseACL(td.spec)
		if err != nil {
			t.Errorf("parse error for ACL \"%s\": %v", td.spec, err)
			continue
		}

		result := acl.Match(testCreds, ctx)
		if result != td.result {
			t.Errorf("evaluation error for ACL \"%s\": got %v, expected %v", td.spec, result, td.result)
			continue
		}
	}
}
