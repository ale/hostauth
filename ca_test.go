package hostauth

import (
	"crypto/ecdsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"net"
	"testing"
)

func TestCA_CreateRoot(t *testing.T) {
	ca, err := CreateRootCA()
	if err != nil {
		t.Fatal(err)
	}
	if !ca.Certificate.IsCA {
		t.Fatal("CA bit not set")
	}
}

func TestCA_RootIsSelfSigned(t *testing.T) {
	ca, err := CreateRootCA()
	if err != nil {
		t.Fatal(err)
	}
	crt := ca.Certificate
	err = crt.CheckSignatureFrom(crt)
	if err != nil {
		t.Fatal(err)
	}
}

func createTestServerCert(t *testing.T, ca *CA, cn string) (*ecdsa.PrivateKey, *x509.Certificate, error) {
	csr, priv, err := CreateCertificateRequest(pkix.Name{CommonName: cn}, []string{"altname"}, []net.IP{net.ParseIP("127.0.0.1")})
	if err != nil {
		t.Fatalf("could not create CSR for %s: %v", cn, err)
	}
	crt, err := ca.CreateServerCert(csr)
	if err != nil {
		t.Fatalf("could not sign CSR for %s: %v", cn, err)
	}
	return priv, crt, nil
}

func createTestCa(t *testing.T, ca *CA) (*CA, error) {
	csr, priv, err := CreateCertificateRequest(HostCAName("host"), nil, nil)
	if err != nil {
		t.Fatalf("could not create CSR for CA: %v", err)
	}
	crt, err := ca.CreateCA(csr)
	if err != nil {
		t.Fatalf("could not sign CSR for CA: %v", err)
	}
	return NewCA(priv, crt), nil
}

func TestCA_CreateServerCert(t *testing.T) {
	ca, err := CreateRootCA()
	if err != nil {
		t.Fatal(err)
	}

	_, crt, err := createTestServerCert(t, ca, "server")
	if err != nil {
		t.Fatal(err)
	}

	if len(crt.DNSNames) < 1 {
		t.Errorf("generated certificate does not include all hostnames (got %v, expected [altname])", crt.DNSNames)
	}
	if len(crt.IPAddresses) < 1 {
		t.Errorf("generated certificate does not include all IPs (got %v, expected [127.0.0.1])", crt.IPAddresses)
	}

}

func TestCA_CertificateIsProperlySigned(t *testing.T) {
	ca, err := CreateRootCA()
	if err != nil {
		t.Fatal(err)
	}

	_, crt, err := createTestServerCert(t, ca, "server")
	if err != nil {
		t.Fatal(err)
	}

	err = crt.CheckSignatureFrom(ca.Certificate)
	if err != nil {
		t.Fatalf("Signature verification failed: %v", err)
	}
}

func TestCA_CertificateIsProperlySignedByIntermediateCA(t *testing.T) {
	ca, err := CreateRootCA()
	if err != nil {
		t.Fatal(err)
	}

	subca, err := createTestCa(t, ca)
	if err != nil {
		t.Fatal(err)
	}

	_, crt, err := createTestServerCert(t, ca, "server")
	if err != nil {
		t.Fatal(err)
	}

	roots := x509.NewCertPool()
	roots.AddCert(ca.Certificate)
	inter := x509.NewCertPool()
	inter.AddCert(subca.Certificate)
	verifyOpts := x509.VerifyOptions{
		Roots:         roots,
		Intermediates: inter,
	}
	chains, err := crt.Verify(verifyOpts)
	if err != nil {
		t.Fatalf("Signature verification failed: %v", err)
	}
	if len(chains) == 0 {
		t.Fatalf("No trust chains")
	}
	t.Logf("%+v", chains)
}

func BenchmarkCA_CreateCA(b *testing.B) {
	for i := 0; i < b.N; i++ {
		CreateRootCA()
	}
}

func BenchmarkCA_CreateCertificate(b *testing.B) {
	ca, _ := CreateRootCA()
	csr, _, err := CreateCertificateRequest(pkix.Name{CommonName: "client"}, nil, nil)
	if err != nil {
		b.Fatalf("could not create CSR: %v", err)
	}
	for i := 0; i < b.N; i++ {
		ca.CreateClientCert(csr)
	}
}
