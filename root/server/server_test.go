package server

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"log"
	"net"
	"net/http/httptest"
	"net/url"
	"testing"

	"git.autistici.org/ale/hostauth"
	"git.autistici.org/ale/hostauth/client"
	"git.autistici.org/ale/hostauth/root"
	"git.autistici.org/ale/hostauth/util"
)

var testPw = []byte{'f', 'o', 'o'}

func createTestRootCaServer(t *testing.T) (*hostauth.CA, *httptest.Server) {
	rootca, err := hostauth.CreateRootCA()
	if err != nil {
		t.Fatal(err)
	}
	srv := &Server{CA: rootca, encryptionPw: testPw}

	httpsrv := httptest.NewUnstartedServer(srv)
	httpsrv.TLS, _ = srv.TlsConfig()
	httpsrv.StartTLS()
	log.Printf("started server on %s", httpsrv.URL)

	return rootca, httpsrv
}

type testClient struct {
	hostca *hostauth.CA
	roots  *x509.CertPool
}

func (c *testClient) ClientTlsConfig(addr string) (*tls.Config, error) {
	csr, priv, err := hostauth.CreateCertificateRequest(hostauth.NewCredentials().Name(""), nil, nil)
	if err != nil {
		return nil, err
	}
	crt, err := c.hostca.CreateClientCert(csr)
	if err != nil {
		return nil, err
	}
	return client.RawClientTlsConfig(
		util.Hostname,
		priv,
		crt,
		c.hostca.Certificate,
		c.roots,
	), nil
}

func newTestClient(t *testing.T, rootca *hostauth.CA) *testClient {
	roots := x509.NewCertPool()
	roots.AddCert(rootca.Certificate)

	csr, priv, err := hostauth.CreateCertificateRequest(hostauth.HostCAName(util.Hostname), nil, nil)
	if err != nil {
		t.Fatalf("could not create CSR for CA: %v", err)
	}
	crt, err := rootca.CreateCA(csr)
	if err != nil {
		t.Fatalf("could not sign CSR for CA: %v", err)
	}
	return &testClient{hostca: hostauth.NewCA(priv, crt), roots: roots}
}

func urlAddr(u string) string {
	parsed, err := url.Parse(u)
	if err != nil {
		return u
	}
	if _, port, err := net.SplitHostPort(parsed.Host); err == nil {
		return fmt.Sprintf("localhost:%s", port)
	}
	return parsed.Host
}

func TestRoot_Renew(t *testing.T) {
	rootca, httpsrv := createTestRootCaServer(t)
	defer httpsrv.Close()

	tc := newTestClient(t, rootca)
	stub := root.NewRootCAStub(urlAddr(httpsrv.URL), tc)

	newCrt, err := stub.Renew(tc.hostca.Certificate, tc.hostca.PrivateKey)
	if err != nil {
		t.Fatal(err)
	}
	if newCrt == tc.hostca.Certificate {
		t.Fatal("new and old certs are the same!")
	}
}

func TestRoot_Sign(t *testing.T) {
	rootca, httpsrv := createTestRootCaServer(t)
	defer httpsrv.Close()

	tc := newTestClient(t, rootca)
	stub := root.NewRootCASignerStub(urlAddr(httpsrv.URL), tc.roots)

	_, _, err := stub.Sign("testhost", func() ([]byte, error) {
		return testPw, nil
	})
	if err != nil {
		t.Fatalf("Sign() failed: %v", err)
	}
}
