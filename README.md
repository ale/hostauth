
hostauth
========


An authentication mechanism for a homogeneous service environment. It
implements a model of machine-based trust, leveraging the UNIX user
model to a distributed setting: a daemon running on each machine signs
assertions that a user is who he says he is, and these assertions can
be verified by a remote peer and used to implement access controls.

The system implements a PKI structure that can be used to validate
credentials. The PKI can be used to properly implement authentication
using just the TLS protocol, altough an alternative token-based method
is also supported.

Each host-based daemon maintains its own intermediate certification
authority and implements a key discovery protocol, so every host in
the environment should be able to accept incoming TCP connections on a
standard port. The credentials of the root CA should be distributed on
every host, while the creation of the host CA  is still (temporarily)
a manual step.


## Protocol details

The protocol used is TLS 1.2, with a single GCM-mode cipher.
Unfortunately, a fallback to TLS 1.0 is also available in order to
support older clients (Python 2, for instance).

## Interoperability

Applications can speak the hostauth protocol on the local UNIX socket
for the host CA, and obtain the credentials they need to authenticate
with remote services. The generated certificates are compatible with
OpenSSL >= 1.0.1, so they can be used without modification by all
applications that use `libssl`.

### Normal daemons

Most normal daemons will be unable to automatically renew the
credentials before they expire: to provide this functionality one
should use the `pki-sitter` utility, which will obtain credentials
from the local host CA and has the ability to trigger an external
command on changes. See `pki-sitter --help` for further details on its
operation.

### Python 2

A client library for Python is available at the
[py-hostauth][py-hostauth] project.

There are cases, however, when SSL libraries are old enough that they
don't support the standards required by hostauth. A notable case is
Python 2, whose `ssl` library is obsolete, making it impossible to set
up a TLS 1.2 listener.

For this and other similar cases it is possible to separate SSL
termination and authentication from the main application by using
`pki-fcgi`, a lightweight, high-performance HTTPS server for FastCGI
applications (a bit like *gunicorn* or *uwsgi*). Client authentication
details will then be made available to the application via a variable
in the request environment.

The tool can be used with any FastCGI application, not just Python
ones.


## Installation

You can either install the code from source. You will need a Go
development environment, and the `godep` tool to be installed, then:

    $ go get git.autistici.org/ale/hostauth
    $ godep go install git.autistici.org/ale/hostauth/...

Otherwise, you can just install the Debian package `hostauth` from the
`deb.autistici.org/debian` repository.


## Usage

Let's consider a fairly typical case of installing a PKI
infrastructure with `hostauth` on a small cluster of hosts. From the
point of view of authentication, all hosts are identical, and all need
to run the `pki-hostd` host CA daemon. There is a special role though:
somewhere, an instance of `pki-rootd` must also be running. The root
CA daemon sees relatively low traffic, and can be run on separate
infrastructure if desired for security concerns (the root CA daemon
requires a copy of the root CA private key to operate).

First, a new root CA certificate should be created:

    $ pki-catool --init --out-crt=root.crt --out-key=root.key

Store a backup copy of `root.crt` and `root.key` somewhere safe, and
take precautions to preserve their safety and integrity: these are the
key credentials to the whole authentication system.

The root CA certificate should then be converted to a format in which
it can be distributed to all clients. By default, clients will look
for this file in `/etc/hostauth/root.pem`:

    $ openssl x509 -inform DER -in root.crt -outform PEM \
          -out /etc/hostauth/root.pem

This file should be present on all hosts where client and/or servers
are running.

The root CA daemon accepts signature requests from any untrusted host,
but it protects the generated host certificates by encrypting them
with a password. Only clients possessing that password will be able to
decrypt and use the newly generated certificate. This password should
never be present on normal hosts: the administrator will be prompted
for it interactively only when a new host is enrolled.

Start `pki-rootd` somewhere, put the encryption password in
`/etc/hostauth/root.secret`, and pass it the credentials created
above:

    $ pki-rootd --root-key=root.key --root-crt=root.crt

Or, if you are using the Debian package, you can simply just put those
files in the `/var/lib/hostauth` directory and run `service pki-rootd
start` to start the daemon.

The `pki-rootd` daemon has no internal state, so it is possible to run
multiple instances of it (for example for reliability purposes).

By default, the daemon listens on port 2025. You should create
a DNS record to point at it, so that your hosts can find it (and so
that it is relatively easy to migrate it to a different host).

The system is now ready to enroll new hosts.

### Host setup

Every host in your cluster should be running the `pki-hostd` daemon.
Before you run it, though, it will be necessary to initialize the
host-specific intermediate CA certificate:

    $ pki-hostd --init

This will ask you interactively for the certificate decryption
password, and it will save the host certificates in
`/var/lib/hostauth`.

Start the host CA daemon with:

    $ pki-hostd

or `service pki-hostd start` if you are using the Debian package.

Applications can now connect to the host CA daemon using the UNIX
socket at `/var/run/hostauth/local` and request new certificates,
which will contain their signed identities.



[py-hostauth]: https://git.autistici.org/ale/py-hostauth
