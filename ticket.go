package hostauth

import (
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/sha256"
	"crypto/x509"
	"encoding/asn1"
	"encoding/base64"
	"errors"
	"math/big"
	"strings"
	"time"
)

type ecdsaSignature struct {
	R, S *big.Int
}

func unmarshalSignature(data []byte) (*ecdsaSignature, error) {
	var sig ecdsaSignature
	if _, err := asn1.Unmarshal(data, &sig); err != nil {
		return nil, err
	}
	return &sig, nil
}

// A Ticket is a signed assertion about an identity, made by the host
// specified in the contained credentials.
type Ticket struct {
	Credentials Credentials
	Deadline    time.Time
}

// Sign the ticket with the given ECDSA private key.
func (t *Ticket) Sign(key *ecdsa.PrivateKey) (string, error) {
	tenc, err := asn1.Marshal(*t)
	if err != nil {
		return "", err
	}
	r, s, err := ecdsa.Sign(rand.Reader, key, digest(tenc))
	if err != nil {
		return "", err
	}
	encsig, err := asn1.Marshal(ecdsaSignature{R: r, S: s})
	if err != nil {
		return "", err
	}
	str := strings.Join(
		[]string{
			base64.StdEncoding.EncodeToString(tenc),
			base64.StdEncoding.EncodeToString(encsig),
		},
		".")

	return str, nil
}

type PublicKeyLocator interface {
	Get(string) (*x509.Certificate, error)
}

// ParseTicket parses an identity assertion ticket and validates it
// against the public key of the host that asserts to have signed it.
// Verification that the host certificate is signed by the root CA is
// delegated to the PublicKeyLocator.
func ParseTicket(ticket string, keydb PublicKeyLocator) (*Ticket, error) {
	fields := strings.Split(ticket, ".")
	if len(fields) != 2 {
		return nil, errors.New("bad format")
	}

	// Decode the ticket and perform some generic validation.
	enctkt, err := base64.StdEncoding.DecodeString(fields[0])
	if err != nil {
		return nil, err
	}
	var t Ticket
	if _, err := asn1.Unmarshal(enctkt, &t); err != nil {
		return nil, err
	}
	if time.Now().After(t.Deadline) {
		return nil, errors.New("authentication ticket expired")
	}

	// Decode the signature.
	encsig, err := base64.StdEncoding.DecodeString(fields[1])
	if err != nil {
		return nil, err
	}
	var sig ecdsaSignature
	if _, err := asn1.Unmarshal(encsig, &sig); err != nil {
		return nil, err
	}

	// Get the public key from the expected host.
	crt, err := keydb.Get(t.Credentials.Host)
	if err != nil {
		return nil, err
	}
	pub, ok := crt.PublicKey.(*ecdsa.PublicKey)
	if !ok {
		return nil, errors.New("need an ecdsa.PublicKey")
	}

	dgst := digest(enctkt)
	if !ecdsa.Verify(pub, dgst, sig.R, sig.S) {
		return nil, errors.New("ecdsa signature verification failure")
	}

	return &t, nil
}

func digest(data []byte) []byte {
	h := sha256.New()
	h.Write(data)
	return h.Sum(nil)
}
