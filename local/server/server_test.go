package server

import (
	"crypto/x509"
	"testing"

	"git.autistici.org/ale/hostauth"
	"git.autistici.org/ale/hostauth/local"
	"git.autistici.org/ale/hostauth/util"
)

func createTestServer(t *testing.T) *Server {
	rootca, err := hostauth.CreateRootCA()
	if err != nil {
		t.Fatal(err)
	}
	pool := x509.NewCertPool()
	pool.AddCert(rootca.Certificate)

	csr, priv, err := hostauth.CreateCertificateRequest(hostauth.HostCAName(util.Hostname), nil, nil)
	if err != nil {
		t.Fatalf("could not create CSR for CA: %v", err)
	}
	crt, err := rootca.CreateCA(csr)
	if err != nil {
		t.Fatalf("could not sign CSR for CA: %v", err)
	}
	keyData, _ := hostauth.MarshalPrivateKey(priv)
	crtData := crt.Raw
	ca, err := hostauth.NewCAFromBytes(keyData, crtData)
	if err != nil {
		t.Fatalf("could not create CA: %v", err)
	}
	s, err := NewServer(ca, pool, "")
	if err != nil {
		t.Fatal(err)
	}
	return s
}

func TestServer_SignOk(t *testing.T) {
	ca := createTestServer(t)

	creds := hostauth.NewCredentials()
	csr, _, err := hostauth.CreateCertificateRequest(creds.Name(""), nil, nil)
	if err != nil {
		t.Fatal(err)
	}
	req := local.SignRequest{
		CertificateRequest: csr.Raw,
	}
	var resp local.SignResponse
	ucreds := &util.UnixCredentials{User: creds.User}

	ca.Sign(&req, &resp, ucreds)
	if resp.Error != "" {
		t.Fatal(resp.Error)
	}
}

func TestServer_SignFailsCredentialValidation(t *testing.T) {
	ca := createTestServer(t)

	creds := hostauth.NewCredentials()
	creds.Host = "another_host"
	csr, _, err := hostauth.CreateCertificateRequest(creds.Name(""), nil, nil)
	if err != nil {
		t.Fatal(err)
	}
	req := local.SignRequest{
		CertificateRequest: csr.Raw,
	}
	var resp local.SignResponse
	ucreds := &util.UnixCredentials{User: creds.User}

	ca.Sign(&req, &resp, ucreds)
	if resp.Error == "" {
		t.Fatal("no error with mismatched host")
	}

	creds = hostauth.NewCredentials()
	csr, _, err = hostauth.CreateCertificateRequest(creds.Name(""), nil, nil)
	if err != nil {
		t.Fatal(err)
	}
	req = local.SignRequest{
		CertificateRequest: csr.Raw,
	}
	var resp2 local.SignResponse
	ucreds = &util.UnixCredentials{User: "another_user"}

	ca.Sign(&req, &resp2, ucreds)
	if resp2.Error == "" {
		t.Fatal("no error with mismatched user")
	}
}
